#ifndef SECONDE_H
#define SECONDE_H

#include <QDialog>
#include <QLineEdit>
#include <QGroupBox>
#include <QBoxLayout>
#include <QPushButton>

namespace Ui {
    class Seconde;
}

class Seconde : public QDialog
{
    Q_OBJECT

public:
    explicit Seconde(QWidget *parent = 0);
    ~Seconde();



private:
    QLineEdit *numero;
    QLineEdit *nom;
    QLineEdit *numeroPart;
    QLineEdit *score;



    QGroupBox *groupeDefinition;
    QBoxLayout *groupeBoutons;

    QPushButton *quitBtn();
    QPushButton *ajouBtn();
    QPushButton *equiAjouBtn();

    Ui::Seconde *ui;


};

#endif // SECONDE_H
