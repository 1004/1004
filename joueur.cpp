#include "joueur.h"

Joueur::Joueur(QString nom) :
    classement(0),
    total(0)
{
    setNom(nom);
}

void Joueur::setNom(QString n_nom) {
    nom = n_nom;
}
void Joueur::addColonne() {
    tab_score << 0;
}
void Joueur::addColonne(QString score) {
    tab_score << score.toInt();     // return 0 if it fails, which is our default value by the way
}

void Joueur::addColonne(int score)
{
    tab_score << score;
}

void Joueur::insertColonne(int position) {
    tab_score.insert(position, 0);
}
void Joueur::removeColonne(int position) {
    tab_score.remove(position);
}
void Joueur::setClassement(int n_classement) {
    classement = n_classement;
}
void Joueur::clearScore() {
    tab_score.clear();
}

QString Joueur::getScore(int position) const {
    return QString::number(tab_score.value(position, -1));
}

int Joueur::getScoreInt(int position) const {
    return tab_score.value(position, -1);
}

QString Joueur::getNom() const {
    return nom;
}
int Joueur::getSize() const {
    return tab_score.size();
}
int Joueur::getClassement() const {
    return classement;
}

int Joueur::getTotal() const {
    int total = 0;

    for(int i = 0; i < tab_score.size(); i++) {
        total += tab_score[i];
    }

    return total;
}

