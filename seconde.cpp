#include <QIODevice>
#include <QPushButton>
#include <QFormLayout>
#include <QWidget>
#include "seconde.h"
#include "ui_seconde.h"

Seconde::Seconde(QWidget *parent) :
    QDialog(parent)//,
   // ui(new Ui::Seconde)
{
   // ui->setupUi(this);
    //Groupe des définitions de classes
    //QString nom = QInputDialog::getText(this,"Nom",QLineEdit::Normal,QString());
    numero = new QLineEdit();
    nom = new QLineEdit();
    numeroPart = new QLineEdit();
    score = new QLineEdit();

    //QLineEdit nom = QString();
    //numero = ajouterEquipe;


    //Groupe des inscriptions participants
    QFormLayout *definitionLayout = new QFormLayout;
    definitionLayout->addRow("&Numéro du participant :" ,numero);
    definitionLayout->addRow("&Nom du participant :" ,nom);
    definitionLayout->addRow("&Numéro de la partie :" ,numeroPart);
    definitionLayout->addRow("&Score :" ,score);
        //QString nomColonne = QInputDialog::getText(this,"Ajouter une Partie" , "Nom et No de la partie : ",QLineEdit::Normal, QString(), &ok);

    QGroupBox *groupeDefinition = new QGroupBox("Entrez les renseignements demandés");
    groupeDefinition->setLayout(definitionLayout);
  
    QVBoxLayout *layoutPrincipal = new QVBoxLayout;
    layoutPrincipal->addWidget(groupeDefinition);
    setLayout(layoutPrincipal);
    resize(450, 350);
    setPalette(QPalette("light gray","yellow"));
    setWindowTitle("Inscriptions et scores");
    
	
	QHBoxLayout *boutonsLayout = new QHBoxLayout;
        boutonsLayout->setAlignment(Qt::AlignRight);
        resize(450, 210);
	QPushButton *quitBtn = new QPushButton("Quitter",this);
    quitBtn->move(330,170);
	QPushButton *ajouBtn = new QPushButton("Ajouter",this);
        ajouBtn->move(20, 170);
        //connect(ajouBtn,SIGNAL(clicked()),this,SLOT(ajouterEquipe()));
    connect(quitBtn,SIGNAL(clicked()),qApp,SLOT(quit()));
    QPushButton *okBtn = new QPushButton("OK",this);
    okBtn->move(180,170);
    //connect(okBtn,SIGNAL(clicked()),this,SLOT(ajouterEquipe()));
    //connect(okBtn ,SIGNAL(clicked()),this,SLOT(ajouterEquipe()));
    //connect(ajouBtn ,SIGNAL(triggered()),this, SLOT(ajouterEquipe()));

            }



Seconde::~Seconde()
{
    delete ui;
}
