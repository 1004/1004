#include "listejoueurs.h"

// Ajoute un pointeur � notre liste, on doit d�cider si on devient propri�taire de l'objet
// ou non, le documenter bri�vement et ensuite s'y tenir dans le projet.
// Dans ce cas ci, on va devenir propri�taire de l'objet, il faudra donc le d�truire.
void ListeJoueurs::addJoueur(Joueur *n_joueur) {
    m_listeJoueurs << n_joueur;
    trier();
}
void ListeJoueurs::addJoueur(Joueur *n_joueur, int nb_colonne) {
    int position = getSize();

    m_listeJoueurs << n_joueur;
    for(int i = 0; i < nb_colonne; i++) {
        m_listeJoueurs[position]->addColonne();
    }
    trier();
}
void ListeJoueurs::removeJoueur(int position) {
    // on recup�re le joueur avant de le retirer, afin de pouvoir le supprimer (pas de pertes de m�moire)
    Joueur* j = m_listeJoueurs.value(position);
    if (j) {
        m_listeJoueurs.remove(position);
        delete j;
    }
}

void ListeJoueurs::clearAllJoueur() {
    // Ici destruction de tous les joueurs encore pr�sents dans la liste
    foreach (Joueur* j, m_listeJoueurs) delete j;

    m_listeJoueurs.clear();
}

ListeJoueurs::ListeJoueurs(QObject *parent) :
    QAbstractTableModel(parent)
{
}

ListeJoueurs::~ListeJoueurs()
{
    // Ici destruction de tous les joueurs encore pr�sents dans la liste
    foreach (Joueur* j, m_listeJoueurs) delete j;
}

void ListeJoueurs::addColonneAllJoueur(int position) {
    for(int i = 0; i < getSize(); i++) {
        m_listeJoueurs[i]->insertColonne(position);
    }
}
void ListeJoueurs::removeColonneAllJoueur(int position) {
    for(int i = 0; i < getSize(); i++) {
        m_listeJoueurs[i]->removeColonne(position);
    }
}
void ListeJoueurs::changeNom(int position, QString n_nom) {
    m_listeJoueurs[position]->setNom(n_nom);
}
void ListeJoueurs::trier() {
    qSort(m_listeJoueurs.begin(), m_listeJoueurs.end(), caseInsensitiveLessThan);
}
void ListeJoueurs::trierParScore() {
    qSort(m_listeJoueurs.begin(), m_listeJoueurs.end() ,classementPM);
    for(int i = 0; i < getSize(); i++) {
        m_listeJoueurs[i]->setClassement(i + 1);
    }
}

// Attention, quid si deux joueurs ont le m�me nom ? Est-ce v�rifi� par le programme ?
// Je pr�f�res comparer les pointeurs directement dans ce cas.
// De plus QVector a une fonction indexOf()
// Note la fonction n'est pas appel�e, on pourrait aussi la supprimer...
int ListeJoueurs::getPosition(Joueur *n_joueur) {
    return m_listeJoueurs.indexOf(n_joueur);
}
int ListeJoueurs::getSize() {
    return m_listeJoueurs.size();
}
int ListeJoueurs::getClassement(int position) {
    return m_listeJoueurs[position]->getClassement();
}
int ListeJoueurs::getTotal(int position)  {
    return m_listeJoueurs[position]->getTotal();
}

QString ListeJoueurs::getInfoJoueur(int position) {
    QStringList info;
    info << m_listeJoueurs[position]->getNom() ;
    if(m_listeJoueurs[position]->getSize() > 0) {
        for(int i = 0; i < m_listeJoueurs[position]->getSize(); i++) {
            info << m_listeJoueurs[position]->getScore(i);
        }
    }
    return info.join(":");
}
QStringList ListeJoueurs::getTotalInfoJoueur(int position) {
    QStringList info;

    info << m_listeJoueurs[position]->getNom();
    if(m_listeJoueurs[position]->getClassement() == -1)
        info << "" ;
    else {
        info << QString::number(m_listeJoueurs[position]->getClassement());
        info << QString::number(m_listeJoueurs[position]->getTotal());
        for(int i = 0; i < m_listeJoueurs[position]->getSize(); i++) {
            info << m_listeJoueurs[position]->getScore(i);
        }
    }
    return info;
}

bool caseInsensitiveLessThan(const Joueur* j1, const Joueur* j2)  {

    return j1->getNom().toLower() < j2->getNom().toLower();
}
bool classementPM(Joueur* j1, Joueur* j2) {
    return j1->getTotal() > j2->getTotal();
}

/**
 * Row count va permettre � la vue de savoir combien de ligne il y a dans notre table.
 * Ici c'est facile, on a autant de ligne que de joueurs.
 * QModelIndex repr�sente une sorte d'index, de pointeur vers une case de la table,
 * ici on n'en tient pas compte car le mod�le n'est pas un arbre (te tracasse pas pour le moment)
 */
int ListeJoueurs::rowCount(const QModelIndex &) const
{
    // on revoie donc le nombre de joueur pr�sents.
    return m_listeJoueurs.size();
}

int ListeJoueurs::columnCount(const QModelIndex &) const
{
    // le nombre de colonne n'est pas connu si facilement, il faut parcourir la liste de joueurs et calculer le maximum
    int colCount = 0;
    foreach (Joueur* joueur, m_listeJoueurs) {
        colCount = qMax(colCount, joueur->getSize());
    }

    // On ajoute 2 car on a le nom et le total � afficher en d�but de ligne
    return colCount + 2;
}

/**
 * data vas nous permettre de donner les donn�es du mod�le � la vue.
 * en fait c'est la vue qui va nous interroger, apr�s avoir demander la taille de la table via
 * rowCount et columnCount, elle va pour chaque case demander ce qu'il faut afficher !
 */
QVariant ListeJoueurs::data(const QModelIndex &index, int role) const
{
    // Les coordonn�es de la case sont dans l'index fourni
    int row = index.row();
    int col = index.column();

    // On v�rifie d'abord que row est valide, on retourne du vide dans le cas contraire
    if (row < 0 || row >= m_listeJoueurs.size()) return QVariant();

    // Le role permet de configurer l'apparence de chaque case (couleur de fond etc...)
    // Pour l'instant on ne s'int�resse que au "display role" les autres on ne revoie rien
    if (role != Qt::DisplayRole) return QVariant();

    // Premi�re colonne retourne le nom du joueur
    if (col == 0) {
        QString name = m_listeJoueurs[row]->getNom();

        // QVariant est un esp�ce de type polymorphe, on peut y mettre du texte, des entiers, des couleurs, tout un tas de truc.
        return QVariant(name);
    }

    // Deuxi�me colonne le total
    if (col == 1) {
        int total = m_listeJoueurs[row]->getTotal();
        return QVariant(total);
    }

    // Ensuite c'est les scores individuels.
    int position = col - 2;  // on retire 2 pour les deux colonnes pr�c�dentes
    if (position < 0 || position >= m_listeJoueurs[row]->getSize()) return QVariant();
    int score = m_listeJoueurs[row]->getScoreInt(position);
    return QVariant(score);
}

