#ifndef DEF_JOUEUR_H
#define DEF_JOUEUR_H

#include <QString>
#include <QVector>

class Joueur {

public:
         Joueur(QString nom);

         void setNom(QString n_nom);
         void addColonne();
         void addColonne(QString score); // Deprecated
         void addColonne(int score);     // Prefer this
         void insertColonne(int position);
         void removeColonne(int position);
         void setClassement(int n_classement);
         void clearScore();

         QString getNom() const;
         QString getScore(int position) const;
         int getScoreInt(int position) const;
         int getSize() const;
         int getClassement() const;
         int getTotal() const;

private:
         QString nom;
         int classement;
         int total;
         QVector<int> tab_score;
};

#endif // JOUEUR_H
