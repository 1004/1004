#ifndef LISTEJOUEURS_H
#define LISTEJOUEURS_H

#include <QAbstractTableModel>
#include <QVector>
#include <QStringList>
#include "joueur.h"

// On va pr�senter les donn�es sous forme de table, on h�rite de QAbstractTableModel qui fait d�j� une bonne partie du boulot
class ListeJoueurs : public QAbstractTableModel {
public:
    ListeJoueurs(QObject* parent = 0);
    ~ListeJoueurs();

    void addJoueur(Joueur *n_joueur);       // Ajoute un joueur � la liste, on prend la propri�t� de l'objet.
    void addJoueur(Joueur *n_joueur, int nb_colonne);
    void removeJoueur(int position);
    void clearAllJoueur();

    void addColonneAllJoueur(int position);
    void removeColonneAllJoueur(int position);
    void changeNom(int position, QString n_nom);
    void trier();
    void trierParScore();

    int getPosition(Joueur *n_joueur);
    int getSize();
    int getClassement(int position);
    int getTotal(int position);
    QString getInfoJoueur(int position);
    QStringList getTotalInfoJoueur(int position);

    // Fonction reimplement�e de QAbstractModel
public:
    int rowCount(const QModelIndex &) const;
    int columnCount(const QModelIndex &) const;
    QVariant data(const QModelIndex &index, int role) const;

private:
    QVector<Joueur*> m_listeJoueurs;
};

bool caseInsensitiveLessThan(const Joueur *j1, const Joueur *j2);
bool classementPM(Joueur *j1, Joueur *j2);

#endif // LISTEJOUEURS_H
