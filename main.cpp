#include <QtGui/QApplication>
#include <QTextCodec>
#include "fenetre.h"

int main(int argc, char **argv)
{
    QApplication app(argc, argv);
    QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));
    Fenetre fenetre;
    fenetre.show();

    return app.exec();
}
