#-------------------------------------------------
# Créé par François SOLLER
# Project created by QtCreator 2011-07-13T21:34:53
#
# Modifié le 15-07-2012
#-------------------------------------------------

QT       += core gui

TARGET = 1004
TEMPLATE = app



SOURCES += main.cpp \
        fenetre.cpp \
    seconde.cpp \
    joueur.cpp \
    listejoueurs.cpp


HEADERS  += fenetre.h \
    seconde.h \
    joueur.h \
    listejoueurs.h


FORMS    += fenetre.ui \
    seconde.ui



