#include "fenetre.h"
#include "listejoueurs.h"
#include "joueur.h"

Fenetre::Fenetre(QMainWindow *parent) :
    QMainWindow(parent)

{
    setupUi(this);
	
    dlg = new Seconde();

    // Pour tester notre nouveau mod�le - ajout manuel de joueurs et de scores
    Joueur* j1 = new Joueur("Joueur 1");
    j1->addColonne(15);
    j1->addColonne(7);
    j1->addColonne(32);
    groupe.addJoueur(j1);
    Joueur* j2 = new Joueur("Joueur 2");
    j2->addColonne(2);
    j2->addColonne(57);
    j2->addColonne(12);
    groupe.addJoueur(j2);
    tableView->setModel(&groupe);
    // fin test

    modele = new QStandardItemModel(0,3, this);
    listeHeader << QString("Nom") << QString("Total") << QString("Classement");
    modele->setHorizontalHeaderLabels(listeHeader);

    QPushButton *openDlgBtn = new QPushButton ("Ouvrir.",this);
    connect(openDlgBtn, SIGNAL(clicked()),dlg ,SLOT(open()));
    openDlgBtn->move(40, 460);
    QPushButton *quitBtn = new QPushButton ("Quitter.",this);
    connect(quitBtn, SIGNAL(clicked()),qApp ,SLOT(quit()));
    quitBtn->move(340, 460);
	
    connect(ajouParBtn, SIGNAL(clicked()),this , SLOT(ajouterPartie()));
    connect(supParBtn, SIGNAL(clicked()),this , SLOT(supprimerPartie()));

    connect(equiAjouBtn, SIGNAL(clicked()),this , SLOT(ajouterEquipe()));
    connect(equiSupBtn, SIGNAL(clicked()),this , SLOT(supprimerEquipe()));

    connect(actionEnregistrer, SIGNAL(triggered()),this , SLOT(enregistrer()));
    connect(actionEnregistrerSous, SIGNAL(triggered()),this , SLOT(enregistrerSous()));
	
    connect(actionNouveau, SIGNAL(triggered()),this , SLOT(nouveau()));
    connect(actionOuvrir, SIGNAL(triggered()),this , SLOT(ouvrir()));

    connect(modele, SIGNAL(itemChanged(QStandardItem*)),this , SLOT(action(QStandardItem*)));

}
void Fenetre::nouveau() {
	if(groupe.getSize() != 0) {
		groupe.clearAllJoueur(); }
	modele->clear();
	modele->setHorizontalHeaderLabels(listeHeader);
	if(!listeHeaderFollow.isEmpty()) {
                listeHeaderFollow.clear();
                if(actionEnregistrer->isEnabled()) {
                        actionEnregistrer->setEnabled(false); }
        }
}
void Fenetre::ajouterPartie() {
    bool ok = false;
    int numColonne = modele->columnCount();

    QString nomColonne = QInputDialog::getText(this,"Ajouter une Partie" , "Nom et No de la partie : ",QLineEdit::Normal, QString(), &ok);

    if(ok && !nomColonne.isEmpty()) {
        nomColonne = nomColonne.toLower();
        nomColonne.replace(0,1, nomColonne[0].toUpper());
        modele->insertColumn(numColonne);
        modele->setHeaderData(numColonne, Qt::Horizontal, nomColonne);
        listeHeaderFollow << nomColonne;
        groupe.addColonneAllJoueur(numColonne -3);
    }
        else if (ok && nomColonne.isEmpty()) {
            modele->insertColumn(numColonne) ;
            listeHeaderFollow << QString::number(numColonne);
            groupe.addColonneAllJoueur(numColonne -3);
        }
}
void Fenetre::supprimerPartie()  {
	QList<int> listeColonne;
        QModelIndexList liste = tableView->selectionModel()->selectedIndexes();

        for(int i = 0; i < liste.size(); i++)  {
            if(liste[i].column() < 3)  continue;
            else if(listeColonne.contains(liste[i].column())) continue;
            listeColonne << liste[i].column();
                            }
        qSort(listeColonne.begin(),listeColonne.end());
        for(int i = listeColonne.size() - 1; i >= 0; i--) {
            listeHeaderFollow.removeAt(listeColonne[i] - 3);
            modele->removeColumn(listeColonne[i]);
            groupe.removeColonneAllJoueur(listeColonne[i] - 3);
            groupe.trierParScore();
            groupe.trier();
            for(int i = 0; i < groupe.getSize(); i++)  {
                modele->setItem(i, 2, new QStandardItem(QString::number(groupe.getClassement(i))));
            }
        }
}
void Fenetre::ajouterEquipe() {
    bool ok_nom ;
    QString nom ;

    nom  = QInputDialog::getText(this,QString("Ajouter une Equipe"),QString("Entrez le Nom des équipiers"),QLineEdit::Normal,QString(), &ok_nom);
    //score  = QInputDialog::getText(this,QString("Ajouter une Equipe"),QString("Entrez le score des équipiers"),QLineEdit::Normal,QString(), &ok_score);
    if(ok_nom && !nom.isEmpty()) {

        disconnect(equiAjouBtn, SIGNAL(clicked()),this , SLOT(ajouterEquipe()));
        majPremierCaractere(nom);
        groupe.addJoueur(new Joueur(nom),listeHeaderFollow.size());
        chargerTableDansModele();
        connect(equiAjouBtn, SIGNAL(clicked()),this , SLOT(ajouterEquipe()));
    }
}
void Fenetre::supprimerEquipe() {
    QList<int> listeLigne;
    QModelIndexList liste = tableView->selectionModel()->selectedIndexes();

    for(int i =0; i < liste.size(); i++)  {
        if(listeLigne.contains(liste[i].row())) continue;
        listeLigne << liste[i].row();
    }
    qSort(listeLigne.begin(), listeLigne.end());
    for(int i = listeLigne.size() - 1; i >= 0; i--)  {
        modele->removeRow(listeLigne[i]);
        groupe.removeJoueur(listeLigne[i]);
    }
    groupe.trierParScore();
    groupe.trier();
    for(int i = 0; i < groupe.getSize(); i++) {
        modele->setItem(i, 2, new QStandardItem(QString::number(groupe.getClassement(i))));
    }
}
void Fenetre::ouvrir() {
    QString nomFichierSav = nomFichier;
    nomFichier = QFileDialog::getOpenFileName(this, QString("Sélectionnez un fichier"),QString(),"Fichiers *.txt;;Tous les fichiers *");
    if(nomFichier.isEmpty()) {
        nomFichier = nomFichierSav;
    }
    else {
        QFile fichier(nomFichier);
        if(fichier.open(QIODevice::ReadOnly |  QIODevice::Text)) {
            groupe.clearAllJoueur();
            listeHeaderFollow.clear();
            modele->clear();

            int compteur = 0;
            QString texte;
            QStringList data;
            QTextStream flux(&fichier);

            while(!flux.atEnd()) {
                texte = flux.readLine();
                if(compteur == 0) {
                    listeHeaderFollow = texte.split(":");
                    compteur++;
                }
                else {
                    data = texte.split(":");
                    Joueur *joueur = new Joueur(data[0]/*,data[1]*/);
                    for(int i = 2; i < data.size(); i++) {
                        joueur->addColonne(data[i]);
                    }
                    groupe.addJoueur(joueur);
                }
            }
            fichier.close();
            groupe.trierParScore();
            groupe.trier();
            disconnect(modele , SIGNAL(itemChanged(QStandardItem*)),this ,SLOT(action(QStandardItem*)));
            modele->setHorizontalHeaderLabels(listeHeader + listeHeaderFollow);
            chargerTableDansModele();
            if(!actionEnregistrer->isEnabled()) {
                actionEnregistrer->setEnabled(true);
                connect(modele , SIGNAL(itemChanged(QStandardItem*)),this ,SLOT(action(QStandardItem*)));
            }
            else {
                QMessageBox::critical(this,"Erreur","Impossible de charger les informations du fichier" + nomFichier);
            }
        }
    }
}
void Fenetre::imprimer(QTableWidget *tableau_a_imprimer, QString monFichier)  {
    QPrinter *printer = new QPrinter(QPrinter::HighResolution);
    printer->setPaperSize(QPrinter::A4);
    printer->setOutputFormat(QPrinter::PdfFormat);
    printer->setOrientation(QPrinter::Portrait);
    printer->setFullPage(true);

    QPrintDialog printDialog(printer, this);
    if(printDialog.exec() == 1) 
	{
        QTextBrowser * editor = new QTextBrowser;  
           
         //creation de formats d'écriture  
                QTextCharFormat NormalFormat;  
          
         QTextCharFormat ItalicFormat;  
         ItalicFormat.setFontItalic(true);  
           
        //On insere la date et l'heure actuelle au début de la premiere page  
         QDate date;  
         QTime time;  
         date = date.currentDate();  
         time = time.currentTime();  
         QString modif ="\nFait le :\t" + date.toString("dddd dd MMMM yyyy") + " a " + time.toString();  
   
               //changement du format d'ecriture  
         editor->setCurrentCharFormat(ItalicFormat);  
         editor->setAlignment(Qt::AlignLeft); 
		
               //ajout de notre QString a l'endroit du curseur  
         editor->append(modif);  
   
         editor->setCurrentCharFormat(NormalFormat);  
           
         //on insere le titre du tableau  
         QTextCharFormat format_gros_titre;  
         format_gros_titre.setFontPointSize(20);  
         format_gros_titre.setFontWeight(QFont::Bold);  
         format_gros_titre.setVerticalAlignment(QTextCharFormat::AlignMiddle);  
         format_gros_titre.setUnderlineStyle(QTextCharFormat::SingleUnderline);  
   
         QString title ="\n"+QString::fromUtf8(title.toStdString().c_str())+"\n";
   
         editor->setCurrentCharFormat(format_gros_titre);  
         editor->setAlignment(Qt::AlignCenter);  
   
         editor->append(title);  
   
         editor->setCurrentCharFormat(NormalFormat);  
           
         //on crée un curseur a l'endroit du curseur actuel  
         QTextCursor cursor = editor->textCursor();  
         cursor.beginEditBlock();          
           
         //Creation du format du tableau qui sera imprimer  
         QTextTableFormat tableFormat;  
         tableFormat.setAlignment(Qt::AlignHCenter);  
         tableFormat.setAlignment(Qt::AlignLeft);  
         tableFormat.setBackground(QColor("#ffffff"));  
         tableFormat.setCellPadding(5);  
         tableFormat.setCellSpacing(5);  
           
         //Creation du tableau qui sera imprimé avec le nombre de colonne   
         //et de ligne que contient le tableau mis en parametre

         QTextTable *tableau = cursor.insertTable(tableau_a_imprimer->rowCount()+1, tableau_a_imprimer->columnCount(), tableFormat);
           
         QTextFrame *frame = cursor.currentFrame();  
         QTextFrameFormat frameFormat = frame->frameFormat();  
         frameFormat.setBorder(0);  
         frame->setFrameFormat(frameFormat);  
           
         //Format des HEADER du tableau  
         QTextCharFormat format_entete_tableau;  
         format_entete_tableau.setFontPointSize(15);  
         format_entete_tableau.setFontWeight(QFont::Bold);  
           
         //Format du texte des cellules du tableau  
         QTextCharFormat format_cellule;  
         format_cellule.setFontPointSize(12);  
           
         //on ecrit les HEADERS du tableaux dans le tableau a imprimer  
         for ( int i = 0 ; i < tableau_a_imprimer->columnCount() ; i++ )  
         {  
             //on selectionne la premiere cellule de chaque colonne  
             QTextTableCell titre = tableau->cellAt(0,i);  
               
             //on place le curseur a cet endroit  
             QTextCursor cellCursor = titre.firstCursorPosition();  
               
             //on écrit dans la cellule  
             cellCursor.insertText(tableau_a_imprimer->horizontalHeaderItem(i)->text(),format_entete_tableau);  
         }  
           
         QTextTableCell cell;  
         QTextCursor cellCursor;  
           
         for (int row = 1; row < tableau->rows(); row ++)  
             for (int col = 0; col < tableau->columns(); col ++)  
             {  
                 cell = tableau->cellAt(row,col);  
                 cellCursor = cell.firstCursorPosition();  
                   
                 cellCursor.insertText(tr("%1").arg(tableau_a_imprimer->item(row-1,col)->text()),format_cellule);  
             }  
           
         //fin de l'edition  
                cursor.endEditBlock();  
  
                //impression de notre editor dans le QPrinter initialisé au début de la fonction  
         editor->print(printer);  
  

    }
}

void Fenetre::action(QStandardItem *objet) {
    if(objet->column() == 0) {
        QString nom = objet->text();
        majPremierCaractere(nom);
        if(nom != objet->text()) {
            disconnect(modele, SIGNAL(itemChanged(QStandardItem*)), this, SLOT(action(QStandardItem*)));
            objet->setText(nom);
            connect(modele, SIGNAL(itemChanged(QStandardItem*)), this, SLOT(action(QStandardItem*)));
        }

        groupe.changeNom(objet->row(), nom);
    }

    if(objet->column() > 3) {
        QString texte = objet->text();
        double score;
        bool ok;

        score = texte.toDouble(&ok);
        if(ok) {
            if(score >= 0 && score <= 1000) {
                groupe.trierParScore();
                groupe.trier();
                for(int i = 0; i < groupe.getSize(); i++) {
                    modele->setItem(i, 2, new QStandardItem(QString::number(groupe.getClassement(i))));
                }
            }
            else {
                disconnect(modele, SIGNAL(itemChanged(QStandardItem*)), this, SLOT(action(QStandardItem*)));
                objet->setText("");
                QMessageBox::warning(this, QString::fromUtf8("Avertissement"), QString::fromUtf8("Le score doit être comprise entre 0 et 1000 inclus"));
                connect(modele, SIGNAL(itemChanged(QStandardItem*)), this, SLOT(action(QStandardItem*)));
            }
        }

    }
}

void Fenetre::enregistrerSous() {
    nomFichier = QFileDialog::getSaveFileName(this, QString::fromUtf8("Sélectionner un fichier :"), QString(),"Fichiers *.txt;;Tous les fichiers *");
    funcPrivateSav();
}
void Fenetre::enregistrer() {
    funcPrivateSav();
}
void Fenetre::funcPrivateSav() {
    if(!nomFichier.isEmpty()) {
        QFile fichier(nomFichier);
        if(fichier.open(QIODevice::WriteOnly |  QIODevice::Truncate   | QIODevice::Text)) {
            QTextStream flux(&fichier);
			
            flux << listeHeaderFollow.join(":") << "\n";

			groupe.trier();
			
        for(int i = 0; i < groupe.getSize(); i++) {
            flux << groupe.getInfoJoueur(i) << "\n";
        }
                    fichier.close();
            if(!actionEnregistrer->isEnabled()) {
                actionEnregistrer->setEnabled(true);
            }
    }
        else {
            QMessageBox::critical(this, "Erreur" , "Impossible d'enregistrer les informations dans :" + nomFichier);
        }
      }
    }
void Fenetre::chargerTableDansModele() {
    modele->clear();
    modele->setHorizontalHeaderLabels(listeHeader + listeHeaderFollow);
    for(int i = 0; i < groupe.getSize(); i++) {
        QStringList info = groupe.getTotalInfoJoueur(i);
        for(int j = 0; j < info.size(); j++) {
            modele->setItem(i, j, new QStandardItem(info.at(j)));
        }

    }
}
void Fenetre::majPremierCaractere(QString& mot) {
    QStringList listeMot, listeBis;
    listeMot = mot.split(" ");
    for(int i = 0; i < listeMot.size(); i++) {
        mot = listeMot.at(i);
        if(mot.isEmpty())
            continue;
        mot.replace(0,1, mot[0].toUpper());
        listeBis << mot;
    }
    mot = listeBis.join(" ");
}
