#ifndef FENETRE_H
#define FENETRE_H

#include <QtGui>

#include "seconde.h"
#include "ui_fenetre.h"
#include "listejoueurs.h"




class Fenetre : public QMainWindow, private Ui::MainWindow
{
    Q_OBJECT

public:
    Fenetre(QMainWindow *parent = 0);


private slots:

    void showSecondeModal() {
        dlg->setModal(true);
        dlg->show();
    }

        void nouveau();
        void ouvrir();
        void imprimer(QTableWidget * tableau_a_imprimer, QString modele);
        void ajouterPartie();
        void supprimerPartie();
        void ajouterEquipe();
        void supprimerEquipe();
        void enregistrer();
        void enregistrerSous();
        void action(QStandardItem *objet);

private:
        Seconde *dlg;
        QPushButton *openDlgBtn;
        QPushButton *quitBtn;

    QStandardItemModel *modele;
    QStringList listeHeader;
    QStringList listeHeaderFollow;
    QString nomFichier;
    ListeJoueurs groupe;


    void funcPrivateSav();
    void chargerTableDansModele();
    void majPremierCaractere(QString& mot);

};

#endif // FENETRE_H
